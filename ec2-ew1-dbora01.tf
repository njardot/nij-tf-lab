#variable "ami_centos" { default = "ami-0ff760d16d9497662" }                # For CentOS 7 1901 Ireland (ENA enabled)
#variable "ami_centos" { default = "ami-04cf43aca3e6f3de3" }               # For CentOS 7 1901 Frankfurt (ENA enabled)
variable "aws_keypair" {default = "nij_admin" }
variable "disk_size" { default = "15" }
variable "region" { default = "eu-west-1" }

variable "instance_name" { default = "ec2-ew1-dbora01" }           # TODO: check that it's updated per env
variable "dns_domain" { default = "it.dbi-services.com" }                 # TODO: check that it's updated per env
variable "instance_type" { default = "t2.micro" }                 # TODO: check that it's updated per env

variable "tag_owner" { default = "dbi-nij" }
variable "tag_businesscase" { default = "dbi xChange" }
variable "tag_application" { default = "Oracle" }

variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}

provider "aws" {
  region = var.region
  version = "~> 2"
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
}

data "aws_vpc" "dbi_vpc" {
  filter {
    name   = "tag:Name"
    values = ["VPC-dbi"]       # insert value here
  }
}

data "aws_subnet" "private_subnet" {
  vpc_id = "${data.aws_vpc.dbi_vpc.id}"
  filter {
    name   = "tag:Name"
    values = ["dbi-lab-private-a"]       # insert value here
  }
}

# data "aws_ami" "centos7" {
#   most_recent = true
#   owners = [ "aws-marketplace" ]
#   filter {
#     name = "state"
#     values = ["available"]
#   }
#   filter {
#     name = "architecture"
#     values = ["x86_64"]
#   }
#   filter {
#     name = "product-code"
#     values = ["aw0evgkw8e5c1q413zgy5pjce"]
#   }
# }

data "aws_ami" "ol7" {
  most_recent = true
  owners = [ "131827586825" ]
  filter {
    name = "state"
    values = ["available"]
  }
  filter {
    name = "architecture"
    values = ["x86_64"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name = "name"
    values = ["OL7*"]
  }
}

resource "aws_iam_policy" "iam_pol_s3_read_sw" {
  name = "S3-Read-SoftwareLibrary"
  description = "Grant read access to the bucket dbi-nij-software to download software"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
          "Action": [
              "s3:PutObject",
              "s3:GetObject",
              "s3:ListBucket",
              "s3:HeadBucket"
          ],
            "Effect": "Allow",
            "Resource": [
                "arn:aws:s3:::dbi-nij-software",
                "arn:aws:s3:::dbi-nij-software/*",
                "arn:aws:s3:::dbi-nij-oracle",
                "arn:aws:s3:::dbi-nij-oracle/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role" "iam_rol_s3_read_sw" {
  name = "Role-EC2-S3-Read-SoftwareLibrary"
  description = "Allows EC2 instances to access S3 bucket for software download"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "iam_inst_profile_s3_read_sw" {
  name = "${aws_iam_role.iam_rol_s3_read_sw.name}"
  role = "${aws_iam_role.iam_rol_s3_read_sw.name}"
}

resource "aws_iam_role_policy_attachment" "attach_s3_policy" {
  role       = "${aws_iam_role.iam_rol_s3_read_sw.name}"
  policy_arn = "${aws_iam_policy.iam_pol_s3_read_sw.arn}"
}

resource "aws_instance" "ec2_oracle" {
  ami = "${data.aws_ami.ol7.id}"
  instance_type = "${var.instance_type}"
  vpc_security_group_ids = ["${aws_security_group.sg_aws_global.id}"]
  subnet_id = "${data.aws_subnet.private_subnet.id}"
  key_name = "${var.aws_keypair}"
  iam_instance_profile = "${aws_iam_instance_profile.iam_inst_profile_s3_read_sw.name}"

  tags = {
      Name = "${var.instance_name}"
      Owner = "${var.tag_owner}"
      BusinessCase = "${var.tag_businesscase}"
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "${var.disk_size}"
    delete_on_termination = true
  }

  user_data = <<EOF
#cloud-config
hostname: ${var.instance_name}
fqdn: ${var.instance_name}.${var.dns_domain}
timezone: Europe/Zurich
EOF
}

resource "aws_ebs_volume" "ebs_u01" {
  availability_zone = "${aws_instance.ec2_oracle.availability_zone}"
  type = "gp2"
  size = "20"

  tags = {
      Name = "${var.instance_name}_/dev/sdf"
      Owner = "${var.tag_owner}"
      BusinessCase = "${var.tag_businesscase}"
  }
}

resource "aws_ebs_volume" "ebs_u02" {
  availability_zone = "${aws_instance.ec2_oracle.availability_zone}"
  type = "gp2"
  size = "30"

  tags = {
      Name = "${var.instance_name}_/dev/sdg"
      Owner = "${var.tag_owner}"
      BusinessCase = "${var.tag_businesscase}"
  }
}

resource "aws_volume_attachment" "att_ebs_u01" {
  device_name = "/dev/sdf"
  volume_id   = "${aws_ebs_volume.ebs_u01.id}"
  instance_id = "${aws_instance.ec2_oracle.id}"
}

resource "aws_volume_attachment" "att_ebs_u02" {
  device_name = "/dev/sdg"
  volume_id   = "${aws_ebs_volume.ebs_u02.id}"
  instance_id = "${aws_instance.ec2_oracle.id}"
}

output "private_ip" {
  value = aws_instance.ec2_oracle.private_ip
}
