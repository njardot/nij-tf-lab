variable "sg_name_global" { default = "ew1-sg-dbora" }

### Security group
resource "aws_security_group" "sg_aws_global" {
  name        = "${var.sg_name_global}"
  description = "Security group for the ${var.tag_application}"
  vpc_id = "${data.aws_vpc.dbi_vpc.id}"

  tags = {
    Name = "${var.sg_name_global}"
    Owner = "${var.tag_owner}"
    BusinessCase = "${var.tag_businesscase}"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [ "172.22.0.0/16", "192.168.1.0/24", "10.2.1.0/24", "10.29.0.0/24", "212.147.76.209/32" ]
  }

  ingress {
    from_port   = 1521
    to_port     = 1521
    protocol    = "tcp"
    cidr_blocks = [ "172.22.0.0/16", "192.168.1.0/24", "10.2.1.0/24", "10.29.0.0/24" ]
  }

  ingress {
    from_port   = "-1"
    to_port     = "-1"
    protocol    = "icmp"
    cidr_blocks = [ "172.22.0.0/16", "192.168.1.0/24", "10.2.1.0/24", "10.29.0.0/24" ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
